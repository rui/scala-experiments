
![felix](docs/felix.png)

# Scala experiments
[![pipeline status](https://gitlab.com/ruivieira/scala-experiments/badges/master/pipeline.svg)](https://gitlab.com/ruivieira/scala-experiments/-/commits/master) ![scala-2.13](docs/scala-2.13.svg)

Experimenting with Scala.

## Projects

- [`CounterfactualFairness`](scala/src/main/scala/ml/fairness/CounterfactualFairness.scala), Counterfactually fair models
- [`Metrics`](scala/src/main/scala/ml/fairness/Metrics.scala), Fairness disparity metrics
- [`RandomForestProvider`](scala/src/main/scala/ml/smile/providers/RandomForestProvider.scala), SMILE random forest PredictionProvider
- [`LDAProvider`](scala/src/main/scala/ml/smile/providers/LDAProvider.scala), SMILE Linear Discriminant Analysis PredictionProvider
- [`KNNProvider`](scala/src/main/scala/ml/smile/providers/KNNProvider.scala), SMILE KNN PredictionProvider
- [`SMILEPredictionProvider`](scala/src/main/scala/ml/smile/SMILEPredictionProvider.scala), Kogito PredictionProvider for SMILE models
- [`Counterfactual`](scala/src/main/scala/ml/trusty/Counterfactual.scala), TrustyAI counterfactual adapter
- [`AutoReadme`](scala/src/main/scala/scripts/AutoReadme.sc), Automatically generates this README.md
- [`GenericParser`](scala/src/main/scala/parsers/GenericParser.scala), Generic Java parser

---
<sup>Rui Vieira 2022. Logo made with 🅮 images</sup>
