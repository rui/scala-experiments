import $file.Common
import java.nio.file.Paths
import scala.io.Source

import scala.util.matching.Regex

val pattern: Regex = "< relref \"(.*?)\" >".r

val root = Paths.get(System.getProperty("user.home"), "Sync", "code", "sites", "blog-source", "content")
val data = Common.walkTree(root.toFile).filter(f => f.getName.endsWith(".md")).map(f => {
  val text = Source.fromFile(f).getLines
  val links = pattern.findAllIn(text.mkString).matchData.map(m => m.group(1))
  (f, f.getName.dropRight(3), text, links)
})

val backlinks = data.flatMap(d => d._4.map(link => (link, d._2, d._1))).groupBy(b => b._1)

backlinks.foreach(b => {
  val l = b._2.map(_._2).toSet
  println(s"${b._1} => ${l}")
})