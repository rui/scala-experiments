import java.io.File

def walkTree(file: File): Iterable[File] = {
  val children = new Iterable[File] {
    def iterator: Iterator[File] = if (file.isDirectory) file.listFiles.iterator else Iterator.empty
  }
  Seq(file) ++: children.flatMap(walkTree)
}