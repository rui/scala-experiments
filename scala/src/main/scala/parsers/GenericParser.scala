package parsers

import com.github.javaparser.ast.CompilationUnit
import com.github.javaparser.ast.stmt.IfStmt
import com.github.javaparser.ast.visitor.{ModifierVisitor, Visitable}
import com.github.javaparser.utils.SourceRoot

import java.nio.file.Path

// INFO: Generic Java parser
class CountIfStatements extends ModifierVisitor[Void] {
  var count = 0

  override def visit (n: IfStmt, arg: Void):Visitable = {
    System.out.println(n.toString)
    System.out.println("========")
    count += 1
    super.visit(n, arg)
  }
}

class GenericParser(val root: Path) {

  val sourceRoot = new SourceRoot(root)
  val cu: CompilationUnit = sourceRoot.parse("org.kie.kogito.explainability.local.counterfactual", "CounterFactualScoreCalculator.java")
  val counter = new CountIfStatements()
  cu.accept(counter, null)

  println(s"Found ${counter.count} if statements")

}
