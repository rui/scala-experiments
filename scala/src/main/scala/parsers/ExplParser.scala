package parsers

import java.nio.file.{Path, Paths}

object ExplParser {
  def main(args: Array[String]): Unit = {
    val root: Path = Paths.get(System.getProperty("user.home"), "Sync", "code", "rh", "trusty", "kogito-apps", "explainability", "explainability-core").resolve("src/main/java")
    val parser = new GenericParser(root)
  }
}

