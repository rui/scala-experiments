
import org.kie.kogito.explainability.local.counterfactual.{CounterfactualConfig, CounterfactualExplainer, CounterfactualResult, SolverConfigBuilder}
import org.kie.kogito.explainability.model.domain.{FeatureDomain, NumericalFeatureDomain}
import org.kie.kogito.explainability.model.{CounterfactualPrediction, DataDomain, Feature, FeatureFactory, Output, Prediction, PredictionFeatureDomain, PredictionInput, PredictionOutput, Type, Value}
import org.optaplanner.core.config.solver.{EnvironmentMode, SolverConfig}
import org.optaplanner.core.config.solver.termination.TerminationConfig

import java.util
import java.util.UUID
import java.util.concurrent.TimeUnit
import scala.jdk.CollectionConverters._
import scala.util.Random

object Expl {


  val goal: util.List[Output] = List(new Output("inside", Type.BOOLEAN, new Value(true), 0.0d)).asJava
  val random = new Random()
  val randomSeed = 23
  val goalThreshold = 0.01
  val steps = 100_000

  def main(args: Array[String]): Unit = {

    val data: List[(Feature, FeatureDomain, java.lang.Boolean)] = List[Double](100.0, 150.0, 1.0, 2.0)
      .zipWithIndex
      .map(k =>
        (FeatureFactory.newNumericalFeature(s"f-num${k._2}", k._1),
          NumericalFeatureDomain.create(0.0, 1000.0),
        false))
    val features = data.map(_._1)
    val domains = data.map(_._2)
    val constraints = data.map(_._3)

    val dataDomain = new DataDomain(domains.asJava)

    val center = 500.0
    val epsilon = 10.0

    val model = Models.getSumThresholdModel(center, epsilon)

    val terminationConfig = new TerminationConfig().withScoreCalculationCountLimit(steps.toLong)
    val solverConfig = SolverConfigBuilder.builder.withTerminationConfig(terminationConfig).build
    solverConfig.setRandomSeed(randomSeed.toLong)
    solverConfig.setEnvironmentMode(EnvironmentMode.REPRODUCIBLE)
    val counterfactualConfig = new CounterfactualConfig
    counterfactualConfig.withSolverConfig(solverConfig).withGoalThreshold(goalThreshold)
    val explainer = new CounterfactualExplainer(counterfactualConfig)
    val input = new PredictionInput(features.asJava)
    val output = new PredictionOutput(goal)
    val domain = new PredictionFeatureDomain(dataDomain.getFeatureDomains)
    val prediction = new CounterfactualPrediction(input, output, domain, constraints.asJava, null, UUID.randomUUID, null)
    val result = explainer.explainAsync(prediction, model).get(10L, TimeUnit.MINUTES)

    println(features)
    println(result.getEntities)
    println(result.getEntities.asScala.map(f => f.asFeature().getValue.asNumber()).sum)
  }

}
