import ml.fairness.Metrics
import ml.smile.data.Loader
import smile.classification.RandomForest
import smile.data.Tuple
import smile.data.formula.Formula

val df = Loader.csv(Loader.homeRelativePath("data/credit_bias-train.csv").toString)
val subset = df
  .select("NewCreditCustomer", "Amount", "Interest", "LoanDuration", "Education", "PaidLoan")
  .omitNullRows()
  .factorize("Education", "NewCreditCustomer", "PaidLoan")

val target = "PaidLoan"
val protectedAttribute = "Education"

val formula = Formula.lhs(target)
val model = RandomForest.fit(formula, subset)

val metrics = new Metrics[Tuple](model, subset.drop(target))

metrics.parityLoss(protectedAttribute)