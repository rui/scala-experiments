package ml.smile.examples

import smile.classification.KNN

object KNNEx {
  def main(args: Array[String]): Unit = {

    val x = creditBiasDF.select("Amount", "LoanDuration").toArray
    val y = creditBiasDF.select("PaidLoan").toArray.map(y => y(0).toInt)

    val a = KNN.fit(x, y)
    println(a.predict(Array(1000.0, 10.0)))
  }
}
