package ml.smile.examples

import smile.classification.LDA

object LDAEx {
  def main(args: Array[String]): Unit = {

    val x = creditBiasDF.select("Amount", "LoanDuration").toArray
    val y = creditBiasDF.select("PaidLoan").toArray.map(y => y(0).toInt)

    val a = LDA.fit(x, y)
    println(a.predict(Array(1000.0, 10.0)))
  }
}
