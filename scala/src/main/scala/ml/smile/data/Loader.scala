package ml.smile.data

import smile.data.DataFrame
import smile.read

import java.io.File
import java.nio.file.{Path, Paths}

object Loader {

  val root = List("Sync", "code", "scala", "scala-experiments")

  def homeRelativePath(path: String): Path = {
    Paths.get(System.getProperty("user.home"), root :+ path: _*)
  }

  def csv(path: String): DataFrame = {
    read.csv(new File(path).toPath, delimiter = ',', header = true, quote = '"', escape = '\\', schema = null)
  }

}
