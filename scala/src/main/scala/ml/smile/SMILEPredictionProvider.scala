package ml.smile

// INFO: Kogito PredictionProvider for SMILE models
import org.kie.kogito.explainability.model.PredictionProvider

trait SMILEPredictionProvider extends PredictionProvider {

}
