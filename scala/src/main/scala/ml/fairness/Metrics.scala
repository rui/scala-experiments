package ml.fairness

import ml.fairness.BaseMetrics.{Disparity, selectionRate}
import smile.classification.DataFrameClassifier
import smile.data.{DataFrame, _}
import smile.validation.{sensitivity, specificity}

// INFO: Fairness disparity metrics
object Metrics {

  type GroupDisparity = (DataFrame, String, String, String) => Map[String, Double]

  def groupByProtected(data: DataFrame, protectedAttribute: String): Map[String, DataFrame] = {
    data.groupBy(_.getString(protectedAttribute))
  }

  def extendDfWithPredictions(data: DataFrame, model: DataFrameClassifier, predictionColumn: String): DataFrame = {
    val predictions: Array[Array[Int]] = model.predict(data).map(x => Array(x))
    data.merge(DataFrame.of(predictions, predictionColumn))
  }

  private def calculateDisparity(data: DataFrame, protectedAttribute: String, predicted: String, trueValue: String, disparity: Disparity): Map[String, Double] = {
    val predictedIndex = data.columnIndex(predicted)
    val trueValueIndex = data.columnIndex(trueValue)

    groupByProtected(data, protectedAttribute).map { case (level, df) =>
      (level, disparity(df.column(trueValueIndex).toIntArray, df.column(predictedIndex).toIntArray))
    }
  }

  private def calculateDisparityDifference(data: DataFrame, protectedAttribute: String, predicted: String, trueValue: String, groupDisparity: GroupDisparity): Double = {
    val result = groupDisparity(data, protectedAttribute, predicted, trueValue).values
    Math.abs(result.max - result.min)
  }

  private def calculateDisparityRatio(data: DataFrame, protectedAttribute: String, predicted: String, trueValue: String, groupDisparity: GroupDisparity): Double = {
    val result = groupDisparity(data, protectedAttribute, predicted, trueValue).values
    Math.abs(result.min / result.max)
  }

  def truePositiveRate(data: DataFrame, protectedAttribute: String, predicted: String, trueValue: String): Map[String, Double] = {
    calculateDisparity(data, protectedAttribute, predicted, trueValue, sensitivity)
  }

  def trueNegativeRate(data: DataFrame, protectedAttribute: String, predicted: String, trueValue: String): Map[String, Double] = {
    calculateDisparity(data, protectedAttribute, predicted, trueValue, specificity)
  }

  def truePositiveRateDifference(data: DataFrame, protectedAttribute: String, predicted: String, trueValue: String): Double = {
    calculateDisparityDifference(data, protectedAttribute, predicted, trueValue, truePositiveRate)
  }

  def trueNegativeRateDifference(data: DataFrame, protectedAttribute: String, predicted: String, trueValue: String): Double = {
    calculateDisparityDifference(data, protectedAttribute, predicted, trueValue, trueNegativeRate)
  }

  def truePositiveRateRatio(data: DataFrame, protectedAttribute: String, predicted: String, trueValue: String): Double = {
    calculateDisparityRatio(data, protectedAttribute, predicted, trueValue, truePositiveRate)
  }

  def trueNegativeRateRatio(data: DataFrame, protectedAttribute: String, predicted: String, trueValue: String): Double = {
    calculateDisparityRatio(data, protectedAttribute, predicted, trueValue, trueNegativeRate)
  }

  def equalisedOddsDifference(data: DataFrame, protectedAttribute: String, predicted: String, trueValue: String): Double = {
    Array(truePositiveRateDifference(data, protectedAttribute, predicted, trueValue),
      truePositiveRateDifference(data, protectedAttribute, predicted, trueValue)).max
  }

  def equalisedOddsRatio(data: DataFrame, protectedAttribute: String, predicted: String, trueValue: String): Double = {
    Array(truePositiveRateRatio(data, protectedAttribute, predicted, trueValue),
      truePositiveRateRatio(data, protectedAttribute, predicted, trueValue)).min
  }

  def demographicParity(data: DataFrame, protectedAttribute: String, predicted: String, trueValue: String): Map[String, Double] = {
    calculateDisparity(data, protectedAttribute, predicted, trueValue, selectionRate)
  }

  def demographicParityDifference(data: DataFrame, protectedAttribute: String, predicted: String, trueValue: String): Double = {
    calculateDisparityDifference(data, protectedAttribute, predicted, trueValue, demographicParity)
  }

  def demographicParityRatio(data: DataFrame, protectedAttribute: String, predicted: String, trueValue: String): Double = {
    calculateDisparityRatio(data, protectedAttribute, predicted, trueValue, demographicParity)
  }

}
