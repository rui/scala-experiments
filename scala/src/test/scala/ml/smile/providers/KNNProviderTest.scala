package ml.smile.providers

import ml.trusty.Counterfactual
import org.kie.kogito.explainability.model._
import org.kie.kogito.explainability.model.domain.NumericalFeatureDomain
import smile.classification.KNN

import scala.jdk.CollectionConverters._

class KNNProviderTest extends org.scalatest.funsuite.AnyFunSuite {


  private val x = df.select("Amount", "LoanDuration")
  private val y = df.select("PaidLoan")
  val knn = new KNNProvider(x, y)

  test("Wrapper and SMILE must provide the same prediction") {
    val _x = x.toArray
    val _y = y.toArray.map(y => y(0).toInt)
    val modelSMILE = KNN.fit(_x, _y)

    val p = new PredictionInput(
      List(
        FeatureFactory.newNumericalFeature("Amount", amount),
        FeatureFactory.newNumericalFeature("LoanDuration", duration)
      ).asJava)
    val prediction = knn.predictAsync(List(p).asJava).get()
    val valueWrapper = prediction.get(0).getOutputs.get(0).getValue.asNumber().toInt
    val valueSMILE = modelSMILE.predict(Array(amount, duration))

    assert(valueWrapper == valueSMILE)

  }

  test("Counterfactual test") {
    val goal = List(new Output("PaidLoan", Type.NUMBER, new Value(1.0), 0.0d))

    val features = List(
      FeatureFactory.newNumericalFeature("Amount", amount),
      FeatureFactory.newNumericalFeature("LoanDuration", duration),
    )
    val constraints = List(false, false)
    val domains = List(
      NumericalFeatureDomain.create(0.0, 100000.0),
      NumericalFeatureDomain.create(0.0, 10000.0))

    val explainer = new Counterfactual(knn)
    val result = explainer.explain(goal, features, domains, constraints)
    result.getEntities.asScala.foreach(e => println(e))
  }

}
