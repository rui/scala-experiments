package ml.smile.providers

import ml.trusty.{Counterfactual, LIME}
import org.kie.kogito.explainability.model._
import org.kie.kogito.explainability.model.domain.NumericalFeatureDomain
import smile.classification.RandomForest
import smile.data.DataFrame
import smile.data.formula.Formula

import scala.jdk.CollectionConverters._

class RandomForestProviderTest extends org.scalatest.funsuite.AnyFunSuite {
  val subset: DataFrame = df.select("Amount", "LoanDuration", "PaidLoan")

  val formula: Formula = Formula.lhs("PaidLoan")
  val randomForest = new RandomForestProvider(formula, subset)

  private val DEFAULT_NO_OF_PERTURBATIONS = 1

  test("Wrapper and SMILE must provide the same prediction") {
    val modelSMILE = RandomForest.fit(formula, subset)

    val p = new PredictionInput(
      List(
        FeatureFactory.newNumericalFeature("Amount", amount),
        FeatureFactory.newNumericalFeature("LoanDuration", duration)
      ).asJava)
    val prediction = randomForest.predictAsync(List(p).asJava).get()
    val valueWrapper = prediction.get(0).getOutputs.get(0).getValue.asNumber().toInt
    val valueSMILE = modelSMILE.predict(DataFrame.of(Array(Array(amount, duration)), "Amount", "LoanDuration"))(0)

    assert(valueWrapper == valueSMILE)

  }

  test("Constructing with model or data should give same result") {
    val model = RandomForest.fit(formula, subset)
    val modelWrapper1 = new RandomForestProvider(model)
    val modelWrapper2 = new RandomForestProvider(formula, subset)

    val p = new PredictionInput(
      List(
        FeatureFactory.newNumericalFeature("Amount", amount),
        FeatureFactory.newNumericalFeature("LoanDuration", duration)
      ).asJava)
    val prediction1 = modelWrapper1.predictAsync(List(p).asJava).get().get(0).getOutputs.get(0).getValue.asNumber().toInt
    val prediction2 = modelWrapper2.predictAsync(List(p).asJava).get().get(0).getOutputs.get(0).getValue.asNumber().toInt

    assert(prediction1 == prediction2)

  }

  test("Counterfactual test") {
    val goal = List(new Output("PaidLoan", Type.NUMBER, new Value(1.0), 0.0d))

    val features = List(
      FeatureFactory.newNumericalFeature("Amount", amount),
      FeatureFactory.newNumericalFeature("LoanDuration", duration),
    )
    val constraints = List(false, false)
    val domains = List(
      NumericalFeatureDomain.create(0.0, 100000.0),
      NumericalFeatureDomain.create(0.0, 10000.0))

    val explainer = new Counterfactual(randomForest)
    val result = explainer.explain(goal, features, domains, constraints)
    result.getEntities.asScala.foreach(e => println(e))
  }

  test("LIME test") {
    val features = List(
      FeatureFactory.newNumericalFeature("Amount", amount),
      FeatureFactory.newNumericalFeature("LoanDuration", duration),
    )

    val limeExplainer = new LIME(randomForest)
    val saliency = limeExplainer.explain(features)
    println(saliency)

  }

}
